from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.db.models import Sum
from .util import yearly_balance, yearly_income
from mainapp.models import Account, Person, PeriodicalExpense, Income, Expense, Category
from .forms import PersonForm, AccountForm, IncomeForm, ExpenseForm, CategoryForm
from django.utils import timezone
from django.views import View


class Overview(View):

    def get(self, request):

        context = {
            }

        return render(request, 'mainapp/overview.html', context)


def index(request):
    actual_date = timezone.now()
    default_account = Account.objects.get(name__startswith='My')
    owner = default_account.people.all()[0]
    default_account_funds = default_account.calculate_total(None)

    people_list = default_account.people.all()[:4]
    account_list = Account.objects.all()[:4]

    periodical_expenses = PeriodicalExpense.objects.filter(account=default_account)
    p_expenses_soon = []

    for p_expense in periodical_expenses:
        if p_expense.expense_coming_soon:
            p_expenses_soon.append(p_expense)

    incomes = Income.objects.filter(account=default_account, operation_date__lte=timezone.now())[:4]
    expenses = Expense.objects.filter(account=default_account, operation_date__lte=timezone.now())[:4]

    context = {
        'default_account_funds': default_account_funds,
        'owner': owner,
        'people_list': people_list,
        'account_list': account_list,
        'p_expenses_soon': p_expenses_soon,
        'actual_date': actual_date,
        'incomes': incomes,
        'expenses': expenses
    }

    return render(request, 'mainapp/index.html', context=context)


class Accounts(View):
    def get(self, request):
        accounts_list = Account.objects.order_by('id').all()
        account_form = AccountForm()

        labels = []
        data = []

        top_funds_data = []
        top_funds_labels = []

        income_data = []
        income_labels = []

        expense_data = []

        for account in accounts_list:
            if Income.objects.filter(account_id=account.id).count() > 0:
                result = Income.objects.order_by('-account_id').filter(account_id=account.id).aggregate(Sum('amount'))
                income_data.append(result['amount__sum'])
            else:
                data.append(0)

        for account in accounts_list:
            income_labels.append(account.name)

        for account in accounts_list:
            if Expense.objects.filter(account_id=account.id).count() > 0:
                result = Expense.objects.order_by('-account_id').filter(account_id=account.id).aggregate(Sum('amount'))
                expense_data.append(result['amount__sum'])

        queryset = Account.objects.order_by('-savings')[:3]
        queryset_total_funds = Account.objects.order_by('-total')[:3]

        for acc in queryset_total_funds:
            top_funds_labels.append(acc.name)
            top_funds_data.append(acc.total)

        for acc in queryset:
            labels.append(acc.name)
            data.append(acc.savings)

        context = {
            "labels": labels,
            "data": data,
            "top_funds_labels": top_funds_labels,
            "top_funds_data": top_funds_data,
            "accounts": accounts_list,
            "account_form": account_form,
            "account_top_income": income_data,
            "account_top_income_labels": income_labels
        }

        return render(request, 'mainapp/accounts.html', context=context)

    def post(self, request):
        account_form = AccountForm(request.POST)
        print(request)
        if account_form.is_valid():
            new_account_form = account_form.save()
        return redirect('accounts_mainapp')


def account_detail(request, account_id):
    account = Account.objects.get(pk=account_id)
    incomes_history = Income.objects.order_by('-account_id').filter(account_id=account.id)
    expenses_history = Expense.objects.order_by('-account_id').filter(account_id=account.id)
    categories = Category.objects.all()
    category = request.GET.get('category')
    sort_by = request.GET.get('sort_by')
    balance = account.calculate_total(category)

    if (sort_by == "Descending"):
        incomes_history = Income.objects.order_by('-amount')
        expenses_history = Expense.objects.order_by('-amount')
    elif (sort_by == "Ascending"):
        incomes_history = Income.objects.order_by('amount')
        expenses_history = Expense.objects.order_by('amount')

    filtered_incomes = incomes_history.filter(category__name=category)
    filtered_expenses = expenses_history.filter(category__name=category)

    months, balance_by_month, incomes_by_month, expenses_by_month = yearly_balance(account_id, category)

    paginator = Paginator(incomes_history, 2)
    page = request.GET.get('page')
    history_page = paginator.get_page(page)
    num_of_pages = history_page.paginator.num_pages

    context = {
        "months": months,
        "balance_by_month": balance_by_month,
        "incomes_by_month": incomes_by_month,
        "expenses_by_month": expenses_by_month,
        "acc_id": account_id,
        "name": account.name,
        "income": incomes_history,
        "expenses": expenses_history,
        "pages": num_of_pages,
        "balance": balance,
        "categories": categories,
        "category": category,
        "sort_by": sort_by,
        "f_incomes": filtered_incomes,
        "f_expenses": filtered_expenses
    }

    return render(request, 'mainapp/detailed_account.html', context=context)


def delete_income(request, account_id, income_id):
    income_to_delete = Income.objects.get(pk=income_id)
    income_to_delete.delete()
    return redirect('account_detail_view', account_id=account_id)


def delete_expense(request, account_id, expense_id):
    expense_to_delete = Expense.objects.get(pk=expense_id)
    expense_to_delete.delete()
    return redirect('account_detail_view', account_id=account_id)


class AddIncomeView(View):
    def get(self, request, account_id):
        account = account = Account.objects.get(pk=account_id)
        income_form = IncomeForm(initial={"account": account})

        context = {
            "income_form": income_form,
            "account": account
        }

        return render(request, 'mainapp/add_income.html', context=context)

    def post(self, request, account_id):
        income_form = IncomeForm(request.POST)

        if income_form.is_valid():
            new_income_form = income_form.save(commit=False)
            new_income_form.account = Account.objects.get(pk=account_id)
            income_form.save()

        return redirect('account_detail_view', account_id=account_id)


class AddExpenseView(View):
    def get(self, request, account_id):
        account = Account.objects.get(pk=account_id)
        expense_form = ExpenseForm()

        context = {
            "account": account,
            "expense_form": expense_form
        }

        return render(request, 'mainapp/add_expense.html', context=context)

    def post(self, request, account_id):
        new_expense = ExpenseForm(request.POST)

        if new_expense.is_valid():
            new_expense_form = new_expense.save(commit=False)
            new_expense_form.account = Account.objects.get(pk=account_id)
            new_expense.save()

        return redirect('account_detail_view', account_id=account_id)


class People(View):

    def get(self, request):
        people_list = Person.objects.all()
        person_form = PersonForm()
        data = []
        labels = []

        for person in people_list:
            labels.append(f'{person.name} {person.surname}')

        for person in people_list:
            if Income.objects.filter(whose_income=person.id).count() > 0:
                result = Income.objects.filter(whose_income=person.id).aggregate(Sum('amount'))
                data.append(result['amount__sum'])
            else:
                data.append(0)

        print(labels)
        print(data)

        context = {
            "person_form": person_form,
            "people_list": people_list,
            "top_income": data,
            "top_income_labels": labels,
            "people": people_list
        }

        return render(request, 'mainapp/people.html', context=context)

    def post(self, request):
        new_person_form = PersonForm(request.POST)
        if new_person_form.is_valid():
            new_person = new_person_form.save()

        return redirect('people_mainapp')


def person_detail(request, person_id):
    person = Person.objects.get(pk=person_id)
    incomes_history = Income.objects.order_by('-whose_income_id').filter(whose_income=person_id)
    paginator = Paginator(incomes_history, 5)
    page = request.GET.get('page')
    incomes_history_page = paginator.get_page(page)
    num_of_pages = incomes_history_page.paginator.num_pages

    categories = Category.objects.all()
    category = request.GET.get('category')
    sort_by = request.GET.get('sort_by')

    months, incomes_by_month = yearly_income(person_id, category)

    filtered_incomes = incomes_history.filter(category__name=category)

    print(months, incomes_by_month)

    context = {
        "name": person.name,
        "surname": person.surname,
        "phone_no": person.phone,
        "mail": person.mail,
        "incomes_history_page": incomes_history_page,
        "incomes_history": incomes_history,
        "num_of_pages_list": num_of_pages,
        "months": months,
        "income_each_month": incomes_by_month,
        "categories": categories,
        "category": category,
        "sort_by": sort_by,
        "f_incomes": filtered_incomes,
    }
    return render(request, 'mainapp/detailed_person.html', context=context)


def creators(request):
    context = {
    }

    return render(request, 'mainapp/creators.html', context=context)


class AddCategoryView(View):
    def get(self, request, account_id):
        account = Account.objects.get(pk=account_id)
        category_form = CategoryForm()
        context = {
            "account": account,
            "category_form": category_form
        }
        return render(request, 'mainapp/add_category.html', context)

    def post(self, request, account_id):
        new_category = CategoryForm(request.POST)

        if new_category.is_valid():
            new_category_form = new_category.save()
            new_category_form.account = Account.objects.get(pk=account_id)
            new_category.save()

        return redirect('account_detail_view', account_id=account_id)
