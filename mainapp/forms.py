from django import forms
from django.forms import ModelForm
from .models import Person, Account,Income,Expense,PeriodicalExpense, Category


class IncomeForm(ModelForm):
    class Meta:
        model = Income
        fields = '__all__'
        exclude = ['account',]
        widgets = {
            "amount": forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn'
                }
            ),
            "whose_income": forms.Select(
                attrs={
                    'class': 'form-control',
                    'id':'exampleSelect1'
                }
            ),
            "category": forms.Select(
                attrs={
                    'class': 'form-control',
                    'id': 'exampleSelect1'
                }
            ),
            "operation_date": forms.SelectDateWidget(
                attrs={
                    'class': 'custom-select',
                    'id': 'exampleSelect1',
                    "style": "margin-right:3px"
                }
            ),
            'source': forms.Textarea(
               attrs={
                   'class':'form-control',
                   'id': 'exampleTextarea',
                   'rows': '3',
               }
            ),
        }


class ExpenseForm(ModelForm):
    class Meta:
        model = Expense
        fields = '__all__'
        exclude = ['account',]
        widgets = {
            'amount': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                }
            ),
            "operation_date": forms.SelectDateWidget(
                attrs={
                    'class': 'custom-select',
                    'id': 'exampleSelect1',
                    "style": "margin-right:3px"
                }
            ),
            "purpose": forms.Textarea(
                attrs={
                    'class': 'form-control',
                    'id': 'exampleTextarea',
                    'rows': '3'
                }
            ),
            "category": forms.Select(
                attrs={
                    'class': 'form-control',
                    'id': 'exampleSelect1'
                }
            ),
            "priority_of_operation": forms.Select(
                attrs={
                 'class': 'form-control',
                 'id': 'exampleSelect1'
                }
            )
        }


class PeriodicalExpenseForm(ModelForm):
    class Meta:
        model = PeriodicalExpense
        fields = '__all__'


class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn'
                }
            ),
            'surname': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn'
                }
            ),
            'mail': forms.EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn'
                }
            ),
            'phone': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn'
                }
            )
        }


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = '__all__'
        exclude = ['savings','total']
        widgets = {
            'name': forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn',
                    'style': "margin: 10px"
                }
            ),
            'people': forms.SelectMultiple(
                attrs={
                    'id': "exampleSelect2",
                    "style": "margin-left:10px; width:310px"
                }
            ),
            'savings': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn',
                    'style': "margin: 10px"
                }
            ),
            'total': forms.NumberInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn',
                    'style': "margin: 10px"
                }
            )
        }

class CategoryForm(ModelForm):
    class Meta:
        model = Category
        exclude = ['account', ]
        fields = '__all__'
        widgets = {
            "category": forms.TextInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Enter credentials',
                    'aria-label': 'Add_Person',
                    'aria-describedby': 'add-btn',
                    'style': "margin: 10px"
                }
            )
        }
