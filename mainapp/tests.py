from django.test import TestCase
from django.utils import timezone
import datetime
from .models import Expense,Account,Income,Person
from django.urls import reverse
from . import views


class AccountModelTest(TestCase):
    def test_new_account_without_expenses_and_incomes(self):
        person = Person.objects.create(name="test",surname="Test",mail="x@bot.com", phone="999222111")
        account = Account.objects.create(name="Test")
        self.assertEqual(account.calculate_total(),0)


class ExpenseModelTest(TestCase):

    def test_is_expense_coming_soon_with_future_expense(self):
        time = timezone.now() + datetime.timedelta(days=3)
        future_expesnse = Expense(operation_date=time)
        self.assertIs(future_expesnse.expense_coming_soon(),False)

    def test_is_expense_coming_soon_with_day_after_tomorrow_expense(self):
        time = timezone.now() + datetime.timedelta(days=1, hours=23, minutes=50)
        todays_expense = Expense(operation_date=time)
        self.assertIs(todays_expense.expense_coming_soon(),True)

    def test_is_expense_coming_soon_with_tommorow_expense(self):
        time = timezone.now() + datetime.timedelta(days=1)
        tomorrow_expense = Expense(operation_date=time)
        self.assertIs(tomorrow_expense.expense_coming_soon(),True)

    def test_is_expense_coming_soon_with_past_expense(self):
        time =timezone.now() - datetime.timedelta(seconds=3)
        past_expense = Expense(operation_date=time)
        self.assertIs(past_expense.expense_coming_soon(),False)
