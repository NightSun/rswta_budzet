from django.contrib import admin
from .models import *


class PersonAdmin(admin.ModelAdmin):
    fields = ['surname', 'name', 'mail', "phone"]
    list_display = ('surname', 'name', 'mail', 'phone')

    def person_surname(self, obj):
        return obj.surname

    person_surname.admin_order_field = 'surname'


class AccountAdmin(admin.ModelAdmin):
    fields = ('name', 'people')
    filter_horizontal = ('people',)

    list_display = ('name', 'total')
    list_filter = ('people',)


class IncomeAdmin(admin.ModelAdmin):
    fields = ['account', 'whose_income', 'operation_date', 'source', 'amount']
    list_display = ('source', 'operation_date', 'account', 'amount')
    date_hierarchy = 'operation_date'
    search_fields = ['source']


class ExpenseAdmin(admin.ModelAdmin):
    fields = ['account', 'operation_date', 'purpose', 'priority_of_operation', 'amount']
    list_display = ('purpose', 'operation_date', 'account', 'amount')
    date_hierarchy = 'operation_date'
    list_filter = ('priority_of_operation',)


class PeriodicalExpenseAdmin(admin.ModelAdmin):
    fields = ['account', 'operation_date', 'next_payment_date', 'purpose', 'amount']
    list_display = ('purpose', 'operation_date', 'next_payment_date', 'amount',)
    date_hierarchy = 'operation_date'


admin.site.register(Category)
admin.site.register(Person, PersonAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Income, IncomeAdmin)
admin.site.register(Expense, ExpenseAdmin)
admin.site.register(PeriodicalExpense, PeriodicalExpenseAdmin)
