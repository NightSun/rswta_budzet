from django.urls import path
from . import views
from .views import Overview, People,Accounts,AddIncomeView,AddExpenseView, AddCategoryView
from . import forms

urlpatterns = [
    path('', views.index, name='index_mainapp'),

    # ?sort=<string:category>

    path('accounts/', Accounts.as_view(), name='accounts_mainapp'),
    path('accounts/account=<int:account_id>/', views.account_detail, name='account_detail_view'),
    path('accounts/account=<int:account_id>/filtered_category=<category>', views.account_detail, name='account_detail_view'),
    path('accounts/account=<int:account_id>/add_income', AddIncomeView.as_view(), name='add_income_form'),
    path('accounts/account=<int:account_id>/add_expense',AddExpenseView.as_view(), name='add_expense_form'),
    path('accounts/account=<int:account_id>/add_periodical_expense', views.account_detail, name='add_periodical_expense_form'),
    path('accounts/account=<int:account_id>/add_category', AddCategoryView.as_view(), name='add_category_form'),
    path('accounts/add', Accounts.as_view(),name='add_account'),
    path('accounts/account=<int:account_id>/income=<int:income_id>/delete_income', views.delete_income, name='delete_income'),
    path('accounts/account=<int:account_id>/expense=<int:expense_id>/delete_expense', views.delete_expense, name='delete_expense'),

    path('people/', People.as_view(), name='people_mainapp'),
    path('people/add', People.as_view(), name='add_person'),
    path('people/<int:person_id>/', views.person_detail, name='person_detail_view'),

    path('overview/', Overview.as_view(), name='overview_mainapp'),
    path('creators/', views.creators, name='creators_mainapp'),
]
