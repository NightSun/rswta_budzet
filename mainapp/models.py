from django.db import models
from django.utils import timezone
from datetime import timedelta
from  django.db.models import Sum


class Person(models.Model):
    """
    Model osoby która wpływa na balans budżetu oraz która nim zarządza
    """
    name = models.CharField(max_length=10)
    surname = models.CharField(max_length=15)
    mail = models.EmailField(max_length=30)
    phone = models.CharField(max_length=9)

    def __str__(self):
        return str(self.surname) + " " + str(self.name)


class Account(models.Model):
    """
    Budżet rodzinny
    """
    name = models.CharField(max_length=20,default="My Account")
    total = models.FloatField(default=0.00)
    people = models.ManyToManyField(Person)
    savings = models.FloatField(default=0.00)

    def __str__(self):
        return str(self.name)

    def calculate_total(self, category):
        incomes = Income.objects.filter(account__name=self.name).aggregate(Sum('amount'))["amount__sum"]
        expenses = Expense.objects.filter(account__name=self.name).aggregate(Sum('amount'))["amount__sum"]

        if (category is not None):
            incomes = Income.objects.filter(account__name=self.name, category__name=category).aggregate(Sum('amount'))["amount__sum"]
            expenses = Expense.objects.filter(account__name=self.name, category__name=category).aggregate(Sum('amount'))["amount__sum"]

        if incomes is None:
            incomes = 0
        if expenses is None:
            expenses = 0
        return round(incomes-expenses, 2)


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class CashFlow(models.Model):
    """
    Operacja na budżecie
    """
    amount = models.FloatField(default=0.00)
    operation_date = models.DateField(default=timezone.now)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)


class Expense(CashFlow):
    """
    Wydatek
    """
    HIGH = "HG"
    MEDIUM = 'MD'
    LOW = 'LW'
    PRIORITIES = {
        (HIGH, "High"),
        (MEDIUM, "Medium"),
        (LOW, "Low"),
    }
    purpose = models.TextField()
    priority_of_operation = models.CharField(
        max_length=2,
        choices=PRIORITIES,
        default=LOW,
    )

    def __str__(self):
        return str(self.purpose)

    def expense_coming_soon(self):
        return timezone.now() < self.operation_date < timezone.now() + timedelta(days=2)


class PeriodicalExpense(Expense):
    next_payment_date = models.DateTimeField(default=timezone.now)


class Income(CashFlow):
    """
    Przychód
    """
    source = models.TextField()
    whose_income = models.ForeignKey(Person, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.source)
