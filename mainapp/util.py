from .models import Income,Expense
from django.db.models import Sum
from django.db.models.functions import TruncMonth


def yearly_balance(acc_id, category):
    expenses_each_month = Expense.objects.filter(account_id=acc_id).annotate(month=TruncMonth('operation_date')).values('month').annotate(Sum('amount')).order_by()
    incomes_each_month = Income.objects.filter(account_id=acc_id).annotate(month=TruncMonth('operation_date')).values('month').annotate(Sum('amount')).order_by()
    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    if category is not None:
        expenses_each_month = expenses_each_month.filter(category__name=category)
        incomes_each_month = incomes_each_month.filter(category__name=category)

    incomes_grouped_by_month = [0 for x in range(1,13)]

    for income in incomes_each_month:
        incomes_grouped_by_month.pop(income['month'].month)
        incomes_grouped_by_month.insert(income['month'].month-1,income['amount__sum'])

    expenses_grouped_by_month = [0 for x in range(1,13)]

    for expense in expenses_each_month:
        expenses_grouped_by_month.pop(expense['month'].month)
        expenses_grouped_by_month.insert(expense['month'].month-1, expense['amount__sum'])

    account_balance_grouped_by_month = []

    for (expense,income) in zip(expenses_grouped_by_month,incomes_grouped_by_month):
        account_balance_grouped_by_month.append(income-expense)

    return months, account_balance_grouped_by_month, incomes_grouped_by_month,expenses_grouped_by_month


def yearly_income(person_id, category):
    incomes_each_month = Income.objects.filter(whose_income=person_id).annotate(month=TruncMonth('operation_date')).values('month').annotate(Sum('amount')).order_by()
    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    if category is not None:
        incomes_each_month = incomes_each_month.filter(category__name=category)

    incomes_grouped_by_month = [0 for x in range(1,13)]

    for income in incomes_each_month:
        incomes_grouped_by_month.pop(income['month'].month)
        incomes_grouped_by_month.insert(income['month'].month-1, income['amount__sum'])

    return months, incomes_grouped_by_month
