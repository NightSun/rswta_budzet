function giveDataToChart(data,labels) {

    return {
        type: "doughnut",
        data: {
            datasets: [{
                data: data,
                backgroundColor: [
                    '#690d11', '#06807c', '#5aa90a', '#730ac0', '#D3D3D3'
                ],
                label: 'Population'
            }],
            labels: labels
        },
        options: {
            legend: {
                labels: {
                    fontColor: 'white',
                }
            },
            responsive: true,
            maintainAspectRatio: false
        },
    };
}

function setPlaceForChart(elementid) {
    return document.getElementById(elementid).getContext('2d');
}