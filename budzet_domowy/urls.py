from django.contrib import admin
from django.urls import path, include
from budzet_domowy.views import index
from django.conf.urls import url

urlpatterns = [
    path('admin/', admin.site.urls,name="administrator_acc"),
    path('mainapp/', include('mainapp.urls')),
    path('', index, name='index_main'),

]
